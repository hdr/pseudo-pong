package juego;

import java.awt.Color;

import entorno.Entorno;

public class Slider {

	private double x;
	private double y;

	private double ancho;
	private double alto;
	private Color color;

	private double velocidad;

	public Slider(double x, double y, double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = 150;
		this.alto = 30;
		this.color = Color.YELLOW;
		this.velocidad = velocidad;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(x, y, ancho, alto, 0, color);
	}

	public void moverHaciaLaIzquierda() {
		x -= velocidad;
	}

	public void moverHaciaLaDerecha() {
		x += velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

}
