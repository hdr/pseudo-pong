package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelota;
	private Slider slider;
	private Image fondo;
	
//	private Slider sliderSuperior;
//	private Slider sliderInferior;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Prueba del Entorno", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		// ...

		this.pelota = new Pelota(entorno.ancho() / 2, entorno.alto() / 2, 1);
		this.slider = new Slider(entorno.ancho() / 2, entorno.alto() - 15, 5);
		
		this.fondo = Herramientas.cargarImagen("pasto.png");
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		
		entorno.cambiarFont("serif", 24, Color.RED);
		entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0);

		entorno.escribirTexto("Viva Perón", 400, 300);
		pelota.dibujar(entorno);
		pelota.mover();
		
		slider.dibujar(entorno);
		
		if (entorno.estaPresionada('s')) {
			pelota.acelerar();
		}
		
		if (pelota.chocasteConElEntorno(entorno)) {
			pelota.cambiarDireccion();
//			System.out.println("Viva Perón");
		}
		
		if (pelota.chocasteConElSlider(slider)) {
			pelota.cambiarDireccion();
		}
		
		if (entorno.estaPresionada('a')) {
			slider.moverHaciaLaIzquierda();
			Herramientas.cargarSonido("maldicion.wav").start();
		}
		
		if (entorno.estaPresionada('d')) {
			slider.moverHaciaLaDerecha();
		}
		
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
