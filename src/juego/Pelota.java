package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Pelota {

	private double x;
	private double y;

	private double tamaño; // size
	private Color color;

	private double velocidad;
	private double angulo;
	private Image img;

	public Pelota(double x, double y, double velocidad) {
		this.x = x;
		this.y = y;
		this.tamaño = 50;
		this.color = Color.BLUE;
		this.velocidad = velocidad;
		this.angulo = - Math.PI / 4;
		this.img = Herramientas.cargarImagen("ball.png");
	}

	public void dibujar(Entorno e) {
//		e.dibujarCirculo(x, y, 50, Color.BLUE);
		e.dibujarImagen(img, x, y, angulo, 0.25);
	}

	public void mover() {
		x += velocidad * Math.cos(angulo);
		y += velocidad * Math.sin(angulo);
	}

	public void acelerar() {
		velocidad += 0.5;
	}

	public boolean chocasteConElEntorno(Entorno e) {
		return x - tamaño / 2 < 0 || e.ancho() < x + tamaño / 2 || y - tamaño / 2 < 0;
	}

	public void cambiarDireccion() {
		angulo += Math.PI / 2;
	}

	public boolean chocasteConElSlider(Slider slider) {
		return y + tamaño / 2 > slider.getY() - slider.getAlto() / 2 && slider.getX() - slider.getAncho() / 2 < x && x < slider.getX() + slider.getAncho() / 2;
	}

}
